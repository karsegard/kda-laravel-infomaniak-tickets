<?php
namespace KDA\Infomaniak\API;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class TicketAdminAPI
{
    public static String $url = "https://etickets.infomaniak.com/api/";


    public static function getEvents()
    {
        $url = self::$url."shop/events";
        $req =  Http::withHeaders([
            'key' => config('kda.infomaniak_ticket.sales_key'),
            'credential'=> session('infomaniak_credential')['credential']
        ]);
        $response =  $req->get($url);
        if ($response->status() === 200) {
            return json_decode($response->body(), true);
        } else {
            return false;
        }
    }

    public static function getEventsSummary($existing_events=[], $existing_representations=[])
    {
        $events = self::getEvents();
        $_events = [];
        foreach ($events as $event) {
            $id = $event['group_event_id'];
            $last_imported = 'never';
            if (isset($existing_events[$id][0])) {
                $e = $existing_events[$id][0];
                $last_imported = \Date::parse($e ['imported_on'])->format('j F Y H:i');
            }
            if (!isset($_events[$id])) {
                $_events[$id]=[
                    'id'=>$id,
                    'name'=>$event['name'],
                    'shows'=>[],
                    'last_imported'=> $last_imported,
                    'infomaniak_data'=>self::relevantEventData($event),
                    'infomaniak_event_data' => $event

                ];
            }
            $date = new \DateTime($event['start']);

            $last_imported = 'never';
            if (isset($existing_representations[$event['event_id']][0])) {
                $e = $existing_representations[$event['event_id']][0];
                $last_imported = \Date::parse($e ['imported_on'])->format('j F Y H:i');
            }
            $_events[$id]['shows'][$event['event_id']]=[
                'representation_id'=>$event['event_id'],
                'date'=>$date->format('d.m.Y H:i'),
                'last_imported'=>$last_imported,
                'should_import'=>true,
                'date_infomaniak'=> $event['date'],
                'infomaniak_data'=>self::releventRepresentationData($event),
                'infomaniak_event_data' => $event

            ];
        }
        return $_events;
    }



    public static function relevantEventData($data)
    {
        $keys = [
            'group_event_id'=> 'infomaniak_id',
            'group_event_id'=> 'infomaniak_group_event_id',
            'event_id'=> 'infomaniak_event_id',
            'category'=> 'category',
            'portal'=> 'image',
            'category_id'=>'category_id',
            'name'=>'name'
        ];
        return self::reduceRelevantData($keys, $data);
    }

    public static function releventRepresentationData($data)
    {
        $keys = [
            'date_id'=> 'infomaniak_id',
            'group_event_id'=> 'infomaniak_group_event_id',

            'date'=> 'date',
            'name'=>'name',
            'start'=> 'start',
            'end'=> 'end',
            'shopUrl'=>'ticket_url',
            'status_label'=> ['level'=>'status_label']
        ];
        return self::reduceRelevantData($keys, $data);
    }

    public static function reduceRelevantData($keys, $data)
    {
        //$array = array_intersect_key($keys, $data);
        return collect($data)->intersectByKeys($keys, $data)->reduce(
            function ($result, $value, $key) use ($keys,$data) {
                if ($result ===null) {
                    $result = [];
                }

                $k = $keys[$key];

                //dump($k,$key);
                
                if(!is_array($k)){
                    $result[$k]= $value;

                }else{
                    $result = array_merge($result,self::reduceRelevantData($k,$data[$key]));
                }
                return $result;
            }
        );
    }

    public static function authenticate(Request $request)
    {
        $url = self::$url."shop/login";
        $req =  Http::withHeaders([
            'Key' => config('kda.infomaniak_ticket.sales_key'),
            'user' => $request->input('infomaniak_email'),
            'password' => $request->input('infomaniak_password')
        ]);
        $response =  $req->get($url);
        
        if ($response->status() === 200) {
            self::set_credentials(json_decode($response->body(), true));
            return self::auth();
        } else {
            self::clear_credentials();
            return false;
        }
    }

    public static function clear_credentials()
    {
        return session(['infomaniak_credential'=>null]);
    }
    public static function auth()
    {
        return session('infomaniak_credential');
    }

    public static function set_credentials($creds)
    {
        session(['infomaniak_credential'=> $creds]);
    }

    // no public function to test auth on infomaniak API so it's a crappy hack
    public static function check_sales_auth()
    {
        $existing_creds = session('infomaniak_credential');
        if (empty($existing_creds)) {
            return false;
        }


        $url = self::$url."shop/customers";
        $req =  Http::withHeaders([
            'Key' => config('kda.infomaniak_ticket.sales_key'),
            'credential' => $existing_creds['credential']
            
        ]);
        $response =  $req->get($url);
        if ($response->status() === 200) {
            return true;
        } else {
            return false;
        }
    }
}
