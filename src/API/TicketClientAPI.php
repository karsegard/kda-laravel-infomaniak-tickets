<?php
namespace KDA\Infomaniak\API;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

use KDA\Infomaniak\Collections\ZoneResponse;
use KDA\Infomaniak\Collections\APIResponse;
use KDA\Infomaniak\Collections\Hallmap;
use KDA\Infomaniak\Collections\Order;
use KDA\Infomaniak\Collections\Form;
use KDA\Infomaniak\Collections\Event;
use KDA\Infomaniak\Collections\Customer;

class TicketClientAPI
{
    public static String $url = "https://etickets.infomaniak.com/api/";


    public static String $CREATE_ORDER= 'shop/order/create';


    public static function authenticate(Request $request)
    {

        $response=  self::post("shop/customer/login",[
            'email'=>$request->input('email'),
            'password'=>$request->input('password')
        ]);

        $response = new APIResponse($response,Customer::class);

        if(!$response->hasError()){
            self::set_credentials($response->body->data);
        }
        return $response;
    }

    public static function clear_credentials()
    {
        return session(['infomaniak_customer_credential'=>null]);
    }
    public static function auth()
    {
        return session('infomaniak_customer_credential');
    }

    public static function set_credentials($creds)
    {
        session(['infomaniak_customer_credential'=> $creds]);
    }


    public static function get($url,$args=[])
    {
        $url = self::$url.$url;
        $req =  Http::withHeaders([
            'key' => config('kda.infomaniak_ticket.shop_key')

        ]);
        return  $req->get($url,$args);
    }

    public static function post($url,$data=[],$additional_headers=[])
    {
        $url = self::$url.$url;
        $req =  Http::withHeaders([
            'key' => config('kda.infomaniak_ticket.shop_key'),
            ...$additional_headers
        ]);
        return $req->post($url,$data);
    }

    public static function put($url,$data=[])
    {
        $url = self::$url.$url;
        $req =  Http::withHeaders([
            'key' => config('kda.infomaniak_ticket.shop_key')

        ]);
        return $req->put($url,$data);

    }

    public static function getZone($date_id){
        $response = self::get("shop/event/".$date_id."/zones");
        return new APIResponse($response,ZoneResponse::class);
    }

    public static function getMap($date_id){
        $response = self::get("shop/event/".$date_id."/hall-data");
        return new APIResponse($response,Hallmap::class);
    }

    public static function createOrder(){
        $response =  self::post("shop/order/create",[]);
        return new APIResponse($response);
    }

    public static function updateOrder($order_id,$items){
        return new APIResponse(self::put("shop/order/".$order_id."/tickets",$items),Order::class);
    }

    public static function updateSeats($order_id,$event_id,$items){
        $response =  self::post("shop/order/".$order_id."/".$event_id."/seats",$items);
        return new APIResponse($response,Order::class);
    }

    public static function getCart($order_id){
        $response = self::get("shop/order/".$order_id."/cart");
        return new APIResponse($response,Order::class);
    }

    public static function getOrder($order_id){
        $response = self::get("shop/order/".$order_id);
        return new APIResponse($response,Order::class);
    }

    public static function resetOrderDelay($order_id){
        $response = self::put("shop/order/".$order_id."/delay");
        return new APIResponse($response);
    }

    public static function getPaymentForm($order_id,$args){
        $response = self::get("shop/order/".$order_id."/payment",$args);
        return new APIResponse($response);
    }

    public static function getPaymentFormBank($order_id,$args){
        return self::getPaymentForm($order_id, array_merge($args,['mode'=>'cybermut']));
    }

    public static function getPaymentFormPost($order_id,$args){
        return self::getPaymentForm($order_id,  array_merge($args,['mode'=>'postfinance']));

    }

    public static function getRegisterForm(){
        $response = self::get("shop/customers/form");
        return new APIResponse($response,Form::class);
    }

    public static function getEvent($date_id){
        $response = self::get("shop/event/".$date_id);
        return new APIResponse($response,Event::class);
    }

    public static function createCustomer($customer){
        if($customer['custom']){
            $customer['custom'] = [collect($customer['custom'] )->mapWithKeys(function($item,$key){
                return ["id"=>$key, "values"=>$item];
            })->toArray()];
        }
        $response = self::post("shop/customer/create",$customer);

        $response = new APIResponse($response,Customer::class);
        if(!$response->hasError()){
            self::set_credentials($response->body->data);
        }
        return $response;
    }

    public static function linkCustomer($order_id,$customer_id){
        $response = self::post("shop/order/".$order_id."/customer",["customer_id"=>$customer_id]);
        return new APIResponse($response);
    }

    public static function cancelOperation($order_id,$operation_ref){
        $response = self::put("shop/order/".$order_id."/cancel-operation/".$operation_ref);
        return new APIResponse($response);
    }

    public static function payOnSite($order_id){
        $response = self::post("shop/order/".$order_id."/operations",[
            [
                "payment_id"=>15,
                "amount"=>""
            ]
            ]);

        return new APIResponse($response);
    }
}
