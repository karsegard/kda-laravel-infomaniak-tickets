<?php


namespace KDA\Infomaniak;

use KDA\Infomaniak\Collections\Order;

class Basket {

    protected $order_id ;
    protected $cart = [];
    protected $added_items = [];
    protected $api;

    public function __construct($api){
        $this->api = $api;
        $this->restore();
    }

    public function restore()
    {

        $this->order_id = session('cart') ;
        if(!$this->order_id){
            $this->createOrder();
        }

        $this->recoverOrder();
    }

    protected function save()
    {
        session(['cart'=> $this->order_id]);
    }


    public function createOrder (){
        $response  = $this->api->createOrder();
    //    dd($response);
        $this->order_id = $response->body;
        $this->save();
    }

    protected function recoverOrder (){
        $response = $this->api->getCart($this->order_id);
        if($response->hasError()){
            $this->setOrder(null);
            $this->createOrder();
        }else{
            $this->setOrder($response->body);
        }
    }


    protected function setOrder($data){
        if($data instanceOf Order){
            $this->cart = $data;
        }else{
            $this->cart= new Order($data);
        }
    }

    public function get(){
        return $this->cart;
    }

    public function updateItem($id,$qty){
        $this->added_items[$id]=$qty;
    }

    public function updateSeats($event_id,$seats){

        if(!$this->cart->events){
            throw new \Exception("commande invalide ou expirée");
        }
        $event =  $this->cart->events->where('data.event_id',$event_id)->first();
        if($event){
            $existing_seats = $event->seats();
            //    dump($existing_seats);

            if(count($existing_seats) == count($seats)){

                $res = collect($seats)
                ->reduce(
                    function($order,$seat) use($existing_seats){
                        $order = $existing_seats->groupBy('data.category_id')->reduce( function ($a,$category,$id){
                            $seats = $a['remaining_seats'];
                            $seat = array_pop($seats);
                            if($seat){
                                if(!isset($a['order'][$id])){
                                    $a['order'][$id] = [];
                                }
                                $a['order'][$id][]=$seat;
                            }
                            $a['remaining_seats']=$seats;
                            return $a;
                        },$order);
                        return $order;
                    },['order'=>[],'remaining_seats'=>$seats]
                );

                $res = collect($res['order'])->reduce(function($carry,$item,$key){
                    $carry[]= [
                        'category_id'=> $key,
                        'seats'=>$item
                    ];
                    return $carry;
                },[]);
                return $this->api->updateSeats($this->order_id, $event_id,$res);
            }else{
                throw new \Exception('Seats count mismatch');
            }
        }else{
            throw new \Exception('Event not found');

        }


    }


    public function updateRemote(){

        $newitems = collect($this->added_items)->reduce(function($carry,$item,$key){
            $carry[]= [
                'category_id'=> $key,
                'count'=> $item
            ];
            return $carry;
        },collect([]));

        $existing = $this->cart->categories()->map(function($categorie){
            return [
                'category_id'=>$categorie->data['category_id'],
                'count'=>$categorie->data['count']
            ];
        });

        if($newitems){
            if($existing){
                $newitems = $existing->merge($newitems)->toArray();
            }


            $response = $this->api->updateOrder($this->order_id,$newitems);
            if(!$response->hasError()){
                $this->setOrder($response->body);
            }else{
                return $response;
            }


        }
        return true;
    }


    public function removeEvent($event_id){
        $newitems = collect($this->cart->categories())->reduce(function($carry,$item) use ($event_id){
            $carry[]= [
                'category_id'=> $item->category_id,
                'count'=> ($event_id == $item->event_id) ? 0 : $item->count
            ];
            return $carry;
        },collect([]))->toArray();
        $response = $this->api->updateOrder($this->order_id,$newitems);
        if(!$response->hasError()){
            $this->setOrder($response->body);
        }
        return $response;
    }



}
