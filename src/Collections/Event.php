<?php


namespace KDA\Infomaniak\Collections;




class Event extends NestedObject{
    static public function getAccessorKey(){
        return 'zones';
    }
    static public function getClass(){
        return Zone::class;
    }

    public function categories(){
        return $this->zones->reduce(function($zones,$zone){
            return $zones->merge($zone->categories->map(function($categorie){
                $categorie->data['event_id']=$this->event_id;
                return $categorie;
            },[]));
        },collect([]));
    }

    public function seats(){
        return $this->zones->reduce(function($c,$item){
            return $c->merge($item->seats());
        },collect([]));
    }

    public function ticketsByCategory($merge=[]){
        return $this->categories()->reduce(function($carry,$item){
            $carry[$item->category_id]= [
                'count'=>$item->count,
                'amount'=>$item->amount
            ];
            return $carry;
        },$merge);
    }

    public function hasHallMap (){

        return isset($this->data['hall_id']) && !empty($this->data['hall_id']);
    }


}
