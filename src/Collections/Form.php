<?php


namespace KDA\Infomaniak\Collections;


class CustomField extends NestedObject{

    public function __construct($data){
        parent::__construct($data);
        $this->data['custom'] = true;

        $this->data['placeholder']= $this->id;
    }
}

class Field extends NestedObject{

    public function __construct($data){
        parent::__construct($data);
        $this->data['custom'] = false;
        $this->data['placeholder']= $this->label;

    }
}


class Form {

    public function __construct($data){

        $this->fields = collect($data)->reduce( function ($carry,$item,$key){
            if($key!="custom"){
                $carry->push(new Field(['label'=>$key,'name'=>$key,'type'=>$item]));
            }else{
                $fields = collect($item)->reduce( function($carry,$item,$key){
                    $carry->push(new CustomField(
                        [
                            'label'=>$item['name'],
                            'name'=>'custom['.$item['field_id'].']',
                            'id'=>$item['field_id'],
                            'type'=>$item['status']
                        ]
                    ));
                    return $carry;
                },collect([]) );

                $carry = $carry->merge($fields);
            }
            return $carry;
        },collect([]));

    }
    static public function getAccessorKey(){
        return 'custom';
    }
    static public function getClass(){
        return CustomField::class;
    }

     public function getMinimalFields(){
        return $this->fields->whereIn('data.type',['MANDATORY','visible_customer_reseller']);
    }

}
