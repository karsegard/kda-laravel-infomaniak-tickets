<?php


namespace KDA\Infomaniak\Collections;



class Seat extends NestedObject{

    public function __construct($data){
        parent::__construct($data);

        $this->data['x'] = $this->matrix[2];
        $this->data['y'] = $this->matrix[5];
    }

    public function setParent($parent){
        $this->row = $parent;
    }

    public function coordinatesFrom($xs,$ys){
        $this->data['x']= $xs[$this->data['x']];
        $this->data['y']= $ys[$this->data['y']];
        return $this;
    }



}

class Row extends NestedObject{

    static public function getAccessorKey(){
        return 'seats';
    }
    static public function getClass(){
        return Seat::class;
    }
    public function setParent($parent){
        $this->block = $parent;
    }
}

class Block extends NestedObject{


    static public function getAccessorKey(){
        return 'rows';
    }
    static public function getClass(){
        return Row::class;
    }

    public function getMap(){
        $xs = $this->seats()->sortBy('data.x')->pluck('data.x')->unique()->values()->flip()->toArray();
        $ys = $this->seats()->sortBy('data.y')->pluck('data.y')->unique()->values()->flip()->toArray();

        $this->seats()->map(function($seat) use($xs,$ys){
            $seat->coordinatesFrom($xs,$ys);
        });

        $this->map = [
            'x'=>   $xs,
            'y' => $ys
        ];
        return $this;
    }


    public function seats (){
        return $this->rows->reduce( function ($carry,$row){

            return $carry->merge($row->seats);

        },collect([]) );
    }
}




class Hallmap extends NestedObject{


    static public function getAccessorKey(){
        return 'blocks';
    }
    static public function getClass(){
        return Block::class;
    }
    public function getMap(){
        return $this->blocks->map(function($block){
            return $block->getMap();
        });
    }

}
