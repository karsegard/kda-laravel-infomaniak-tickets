<?php


namespace KDA\Infomaniak\Collections;



class Operation extends NestedObject{


}

class Order extends NestedObject{


/*    static public function getAccessorKey(){
        return 'events';
    }
    static public function getClass(){
        return Event::class;
    }

*/

    static $classes = [
        'events' => Event::class,
        'operations' => Operation::class,
        'customer' => Customer::class
    ];

    static public function getAccessorsKeys(){
        return ['events','operations','customer'];
    }

    static public function getClassByKey($key){
        return static::$classes[$key];
    }

    static public function getAccessorsIsArray($key){
        return $key=='customer'? false:true;
    }

    public function hasCustomer(){
        return (isset($this->data['customer']) && sizeof($this->data['customer']) >0 );
    }
    public function categories(){
        if($this->events){
            return $this->events->reduce( function ($events,$event){
                return $events->merge($event->categories());
            },collect([]));
        }
        return collect([]);
    }

    public function isPaid(){
        return $this->data['status'] === "payment_requested" || $this->data['status'] ==='paid';
    }

    public function isEmpty(){
        return $this->data == null|| $this->data['order_id'] ===null || sizeof($this->events) === 0;
    }
}
