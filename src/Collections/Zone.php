<?php

namespace KDA\Infomaniak\Collections;




class SeatOrder extends NestedObject{
    public function setParent($parent){
        $this->data['category_id']=$parent->category_id;

        $this->parent = $parent;
    }
}


class Categorie extends NestedObject{
    static public function getAccessorKey(){
        return 'seats';
    }
    static public function getClass(){
        return SeatOrder::class;
    }

}

class Zone extends NestedObject{
    static public function getAccessorKey(){
        return 'categories';
    }
    static public function getClass(){
        return Categorie::class;
    }
    public function seats(){
        return $this->categories->reduce(function($c,$item){
            return $c->merge($item->seats);
        },collect([]));
    }
}
