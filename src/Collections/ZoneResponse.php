<?php

namespace KDA\Infomaniak\Collections;



class ZoneResponse extends NestedArray{
    static public function getAccessorKey(){
        return 'zones';
    }
    static public function getClass(){
        return Zone::class;
    }

    public function categories(){
        return $this->zones->reduce(function($zones,$zone){
            return $zones->merge($zone->categories->map(function($categorie){
                return $categorie;
            },[]));
        },collect([]));
    }

    public function ticketsByCategory(){
        return $this->categories()->reduce(function($carry,$item){
            $carry[$item->category_id]= [
                'count'=>0,
                'amount'=>$item->amount
            ];
            return $carry;
        },[]);
    }
}
