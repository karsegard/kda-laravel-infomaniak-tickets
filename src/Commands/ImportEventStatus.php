<?php

namespace KDA\Infomaniak\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use KDA\Infomaniak\Facades\TicketClientAPI;

class ImportEventStatus extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'infomaniak:events:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'publish packages templates';

    /**
     * Get user options.
     */
    protected function getOptions()
    {
        return [
          //  ['create', null, InputOption::VALUE_NONE, 'Create an admin user', null],
        ];
    }

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Publishing the available packages');

         \App\Models\InfomaniakEvent::where('data->date','<=',new \DateTime())->where('data->end_ordering_customer','<=',new \DateTime())->update(['status'=>'closed']);

        $events = \App\Models\InfomaniakEvent::where('data->date','>=',new \DateTime())->where('data->end_ordering_customer','>=',new \DateTime())->get();

        foreach ($events as $event){
            $_event = TicketClientAPI::getEvent($event->infomaniak_id);
            if($_event->body){
                $this->info("updating event ".$event->name." ".$event->date. " to ".$_event->body->status);
                $event->status=$_event->body->status;
                $event->save();
            }else{
                $event->status= "not found";
                $event->save();
            }
        }
        
    }



    /**
     * Get command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
   //         ['email', InputOption::VALUE_REQUIRED, 'The email of the user.', null],
        ];
    }


}
