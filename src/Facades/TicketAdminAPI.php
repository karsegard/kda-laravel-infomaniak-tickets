<?php

namespace KDA\Infomaniak\Facades;

use  Illuminate\Support\Facades\Facade;

class TicketAdminAPI extends Facade{

    protected static function getFacadeAccessor() { return 'ticket_admin'; }
}
