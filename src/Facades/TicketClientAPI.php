<?php

namespace KDA\Infomaniak\Facades;

use  Illuminate\Support\Facades\Facade;

class TicketClientAPI extends Facade{

    protected static function getFacadeAccessor() { return 'ticket_client'; }
}

