<?php

namespace KDA\Infomaniak;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

use KDA\Infomaniak\API\TicketAdminAPI;
use KDA\Infomaniak\API\TicketClientAPI;
use KDA\Infomaniak\Basket;

use KDA\Infomaniak\Commands\ImportEventStatus;

class InfomaniakProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfigs();

        if ($this->app->runningInConsole()) {
            $this->registerConsoleCommands();
        }

        $this->app->singleton('ticket_admin', function ($app) {

            return new TicketAdminAPI();
        });

        $this->app->singleton('ticket_client', function ($app) {

            return new TicketClientAPI();
        });

        $this->app->singleton('basket', function ($app) {

            return new Basket(new TicketClientAPI());
        });

        $this->registerPublishableResources();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router, Dispatcher $event)
    {


    }

    private function registerConsoleCommands()
    {

        $this->commands(Commands\ImportEventStatus::class);
    }


    /**
     * Load helpers.
     */
    protected function loadHelpers()
    {
        foreach (glob(__DIR__ . '/../Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }


    public function registerConfigs()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__, 1) . '/config/kda/infomaniak_ticket.php',
            'kda.infomaniak_ticket'
        );
    }


    /**
     * Register the publishable files.
     */
    private function registerPublishableResources()
    {
        $configPath = dirname(__DIR__, 1) . '/config/kda';

        $publishable = [

            'config' => [
                "{$configPath}/" => config_path('kda')
            ],

        ];
        foreach ($publishable as $group => $paths) {
            $this->publishes($paths, $group);
        }
    }
}
